var pracownicy = [
	{
		"name": "Kowalski Jan",
		"phone": "1234",
		"team": "Zarząd",
		"room": "5",
		"mail": "mail@mail.com",
		"photo": "kowalski"
	},
	{
		"name": "Nowak Maria",
		"phone": "4321",
		"mobile": "501 555 111",
		"team": "Dział Handlowy",
		"role": "Specjalista ds sprzedaży",
		"room": "12",
		"mail": "nowak@mail.com",
		"photo": "nowak"
	},
	{
		"name": "Kot Zofia",
		"phone": "1234",
		"mobile": "600 100 100",
		"team": "Dział Księgowości",
		"role": "Kierownik",
		"room": "12",
		"mail": "kot@mail.com",
		"photo": "kot"
	},
	{
		"name": "Miszewska Katarzyna",
		"phone": "1235",
		"mobile": "600 100 200",
		"team": "Dział Księgowości",
		"room": "12",
		"mail": "miszewska@mail.com",
		"photo": "miszewska"
	}

]